/* REQUIRING PACKAGES, MODELS, MIDDLEWARES AND ROUTES */

const	adminRoutes             = require("./routes/admin"),
        bodyParser				= require("body-parser"),
        cookieParser            = require("cookie-parser"),
        cupRoutes               = require("./routes/cup"),
        entryRoutes             = require("./routes/entry"),
		express 				= require("express"),
		expressSession			= require("express-session"),
		expressSanitizer		= require("express-sanitizer"),
        flash					= require("connect-flash"),
        latarnikRoutes          = require("./routes/latarnik"), 
        localStrategy			= require("passport-local"),
        matchRoutes             = require("./routes/timetable"),
		methodOverride			= require("method-override"),
        mongoose				= require("mongoose"),
        nodemailer              = require("nodemailer"),
		normalRoutes            = require("./routes/normal"),
		passport				= require("passport"),
		User                    = require("./models/user"),
		userRoutes              = require("./routes/user");

/* SETTING AN APP */

const	app = express();

/* SETTING MONGOOSE */

mongoose.set("useFindAndModify", false);
mongoose.connect(process.env.DATABASE, {useNewUrlParser: true, useUnifiedTopology: true});

/* SETTING BASIC STUFF */

app.use(express.json());
app.use(express.static(__dirname + "/public"));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(expressSanitizer());
app.set("view engine", "ejs");
app.use(methodOverride('_method'));
app.use(flash());
app.use(cookieParser());


/* AUTHENTICATION */

app.use(expressSession({
        secret: process.env.HASH,
        resave: false,
        saveUninitialized: false,
        cookie: {
            maxAge: 2678400000
        }
    }));
app.use(passport.initialize());
app.use(passport.session());
    
passport.use(new localStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());


/* SENDING TO SITE */

app.use((req, res, next) =>
{
    res.locals.cookies = req.cookies;
    res.locals.currentUser = req.user;
    res.locals.warning = req.flash("warning");
    res.locals.success = req.flash("success");
    res.locals.error = req.flash("error");
    next();
});

/* ROUTES */

app.use("/", normalRoutes);
app.use("/", userRoutes);
app.use("/entry", entryRoutes);
app.use("/admin", adminRoutes);
app.use("/haxball", cupRoutes);
app.use("/latarnik", latarnikRoutes);
app.use("/timetable", matchRoutes);

// nigth theme cookie set
app.get("/night", (req, res) => 
{
    res.cookie("theme", "Night");
    res.redirect("back");
});

// night theme cookie remove
app.get("/day", (req, res) =>
{
    res.clearCookie("theme");
    res.redirect("back");
});

// default message - 404 error
app.get("*", (req, res) =>
{
    res.render("error");
});


// STARTING A SERVER //

app.listen(process.env.PORT, process.env.IP, () =>
{
	console.log("Server status: ON!");
});