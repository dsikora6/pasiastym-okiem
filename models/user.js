// requiring packages - mongoose and passport
const   mongoose = require("mongoose"),
        passportLocalMongoose = require("passport-local-mongoose");

//defining a schema
const userSchema = new mongoose.Schema({
   username: String, // username as text
   rank: String, // rank as text (User / Admin)
   password: String, // password as text - hashed
   accepted: Boolean, // accepted as boolean - true/false
   comments: [{
       type: mongoose.Schema.Types.ObjectId, // catching comments data as array
       ref: "Comment"
   }]});

userSchema.plugin(passportLocalMongoose); // plugging in passport to schema

//exporting a schema
module.exports = mongoose.model("User", userSchema);