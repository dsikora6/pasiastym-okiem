// requiring mongoose
const mongoose = require("mongoose");

// defining a schema
const matchSchema = new mongoose.Schema({
    unixDate: Number, // number for date in unix time
    date: String, // string for date - YYYY-MM-DD
    teams: [String], // array of teams
    images: [String], // array of teams emblems
    competition: String, // competition name
    competitionImage: String, // competition emblem
    score: String, // score - string as X:Y
    description: String, // competition details
    season: String // which season - string as 202X/YZ
});

// exporting a model
module.exports = mongoose.model("Match", matchSchema);