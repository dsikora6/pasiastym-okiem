// requiring mongoose
const mongoose = require("mongoose");

// defining a schema
const entrySchema = new mongoose.Schema({
    title: String,  // title should be string
    text: String, // also is text
    createdAt: Number, // time of creation is number, unix time
    comments: [
        {
            type: mongoose.Schema.Types.ObjectId, // creating an array of comments, catching id from other collection
            ref: "Comment"
        }]
});

// exporting the schema
module.exports = mongoose.model("Entry", entrySchema);