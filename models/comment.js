// requiring mongoose
const mongoose = require("mongoose");

// defining a schema
const commentSchema = new mongoose.Schema({
    text: String, // text should be string
    author: {
        id: {
            type: mongoose.Schema.Types.ObjectId, // getting userId
            ref: "User"
        },
        username: String // username should be string
    },
    entry: {
        type: mongoose.Schema.Types.ObjectId, // getting entry id
        ref: "Entry"
    },
    createdAt: Number, // number for creation time - unix date
    accepted: Boolean // true/false if comment is accepted
});

// exporting the schema
module.exports = mongoose.model("Comment", commentSchema);