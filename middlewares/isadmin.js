const isAdmin = (req, res, next) =>
{
    // checking, if is logged in
    if(req.isAuthenticated())
    {
        // if is, checking the rank
        if(req.user.rank === "Admin")
        {
            // giving access if is admin
            return next();
        }
        else
        {
            // throwing error if is not admin
            req.flash("error", "Niestety, nie masz uprawnień do odwiedzenia tej strony!");
            res.redirect("/");
        }
    }
    else
    {
        // throwing error if is not logged in
         req.flash("error", "Niestety, nie masz uprawnień do odwiedzenia tej strony!");
        res.redirect("/");
    }
};

// exporting the middleware
module.exports = isAdmin;