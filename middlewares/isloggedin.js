const isLoggedIn = (req, res, next) =>
{
  //checking, if is logged in
  if(req.isAuthenticated()) return next(); // if yes, giving access
  else
  {
    // if not, throwing an error
      req.flash("error", "Musisz być zalogowany by kontynuować!");
      res.redirect("back");
  }
};

// exporting the middleware
module.exports = isLoggedIn;