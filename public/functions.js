// function filling the entry box

const makeEntries = (container, start) => {
  container.innerHTML = ""; // resetting the content of the container
  fetch(`/entry/start/${start}/amount/5`) // making AJAX request
  .then(res => res.json()) // fetching JSON from response
  .then(data => {
    data.forEach(entry => { // for each element
      const li = document.createElement("li"); // creating list item
      li.classList.add("entry__list-item"); // adding a class
      // setting HTML for list item - getting id and title
      li.innerHTML = `<a href="/entry/id/${entry._id}" class="entry__list-link">${entry.title}</a>`;
      container.appendChild(li); // appending the child to the container
    });
  });
};

// function returning total entries amount

const getLength = async () => { // asynchronous
  const length = await  fetch("/entry/all") // making AJAX request
                        .then(res => res.json()) // fetching JSON data
                        .then(data => data.length); // fetching array length
  return length; // returning length as promis - asynchronous function
};

// function sorting all match results

const sortMatches = (data) => {
  data = data.sort((a,b) => // sorting an array
  {
    if(a.unixDate !== b.unixDate) return a.unixDate - b.unixDate; // first look at the date
    else return a.teams[0].localeCompare(b.teams[0]); // if it is the same, return alphabetically
  });
  return data; // returning sorted array
};

// function making entries for aside container

const makeMatchEntry = (match, isToday, forAside) => {
  let date = ""; // empty string for date
  if(isToday) date = moment(match.unixDate).format("HH:mm"); // if match is today, showing only the hour
  else date = moment(match.unixDate).format("YYYY-MM-DD, HH:mm"); // if not today, showing full date
  if(isNaN(parseInt(match.score)) && forAside) match.score = "-"; // if the match result is not specified, and is for aside container, showing the hyphen
  
  const element = document.createElement("div"); // creating div
  element.classList.add("match__container"); // adding the class
  element.classList.add("u-mar-bottom-md"); // adding utility class

  // making empty items
  let awayEmblem = "";
  let competitionDesc = "";
  let competitionEmblem = "";
  let homeEmblem = "";
  let score = "";

  // filing the items

  // emblems
  if(!forAside) awayEmblem = `<img src="${match.images[1]}" onerror="this.src = '/world.png'" class="match__icon">`;
  if(!forAside) competitionEmblem = `<img src="${match.competitionImage}" onerror="this.src = '/world.png'" class="match__icon">`;
  if(!forAside) homeEmblem = `<img src="${match.images[0]}" onerror="this.src = '/world.png'" class="match__icon">`;

  // names
  const awayName = `<span class="match__name">${match.teams[1]}</span>`;
  const competitionName = `<span class="match__name">${match.competition}</span>`;
  const homeName = `<span class="match__name">${match.teams[0]}</span>`;

  // score
  if(!forAside) score = `<span class="match__clubs-hour">${match.score}</span>`;
  else score = `<span>${match.score}</span>`;

  // date
  const dateSpan = `<span class="match__hour">${date}</span>`;

  // description
  if(!forAside) competitionDesc = `<span class="match__name">${match.description}</span>`;

  // making the HTML structure
  element.innerHTML = `${dateSpan}
  <span class="match__clubs">
    <span class="match__clubs-name match__clubs-name--home">
      ${homeEmblem}
      ${homeName}
    </span>
    ${score}
    <span class="match__clubs-name match__clubs-name--away">
      ${awayEmblem}
      ${awayName}
    </span>
  </span>
  <span class="match__competition">
    <span class="match__competition-name">
      ${competitionEmblem}
      ${competitionName}
    </span>
    ${competitionDesc}
  </span>`;
  return element
};

// filling today matches

const matchesTodayBox = document.querySelector("#matchesToday");

if(matchesTodayBox) // if the page shows
{
    fetch(`/timetable/date/${moment().format("YYYY-MM-DD")}`) // making AJAX request
    .then(res => res.json()) // fetching JSON
    .then(data => {
      data = sortMatches(data); // sorting
      if(data.length > 0) matchesTodayBox.innerHTML = ""; // emptying the container if there is a match
      data.forEach(match => {
        const element = makeMatchEntry(match, true, true); // calling function
        matchesTodayBox.appendChild(element); // appending
      })});
}

const showMatches = (data, body) =>
{
  // setting the amount info
  body.innerHTML = `<div class="u-text-center u-mar-top-md u-mar-bottom-md">Znaleziono <strong>${data.length}</strong> wyników.</div>`;
  data = sortMatches(data); // sorting
  data.forEach(match => { // for each match
    const div = makeMatchEntry(match, false, false); // calling the function
    body.appendChild(div); // appending
  });
};

const nextMatchesBox = document.querySelector("#nextMatches"); // selecting the container for next matches

if(nextMatchesBox) // if is on the website
{
    fetch(`/timetable/next/${moment().valueOf()}_8`) // making AJAX request
    .then(res => res.json()) // fetching JSON
    .then(data => {
      data = sortMatches(data); // sorting
      if(data.length > 0) nextMatchesBox.innerHTML = ""; // clearing the container
      data.forEach(match => {
        const element = makeMatchEntry(match, false, true); // for each element calling child
        nextMatchesBox.appendChild(element); // appending
      })}).catch(err => alert("Wystąpił błąd"));; // throwing error
};