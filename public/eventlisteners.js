// selecting elements from website
const   entryBack = document.querySelector("#entryBack"),
        entryList = document.querySelector("#entryList"),
        entryNext = document.querySelector("#entryNext"),
        timetableForm = document.querySelector("#timetableForm");
        
// if the website has the box for the entry list
if(entryList)
{
   let start = 1; // setting start to 1, as the newest article should come first
   makeEntries(entryList, start); // calling the function

   if(entryBack) // for the arrow allowing to view newer articles
   {
      // adding event listener
      entryBack.addEventListener("click", e => {
         start -= 5; // extracting 5 from the start index, as we want 5 newer
         if(start < 1) start = 1; // if the result is lower than 1, set to 1, as it is the newest
         makeEntries(entryList, start); // calling the function
      });
   }

   if(entryNext) // for the arrow allowing to view older articles
   {
      // adding event listener
      entryNext.addEventListener("click", e => {
         start += 5; // adding 5 to the start index, as we want 5 older
         // calling function to get the total number of articles - returning promise as fetch is used
         getLength().then(data => {
            if(start > data) start = data - 4; // if the start index is bigger than the length, extract 4 from total number to get 5 entries
            makeEntries(entryList, start); // calling the function
         });   
      })
   }
}

// if there is a form for a timetable of matches
if(timetableForm)
{
   // selecting all inputs
   const club = document.querySelector("#club"); // input for club option
   const date = document.querySelector("#date"); // input for date option
   const league = document.querySelector("#league"); // input for league option
   const leg = document.querySelector("#leg"); // input for leg option
   const season = document.querySelector("#season"); // input for season option 

   // adding event listener to form
   timetableForm.addEventListener("change", e =>
   {
      // if the selected one is for date, setting display properties for non-used, so they are not visible
      if(e.target.id === "dateRadio")
      {
         season.style.display = "none";
         club.style.display = "none";
         league.style.display = "none";
         leg.style.display = "none";
         date.style.display = "flex";
      }

      // if the selected one is for club, setting display properties for non-used, so they are not visible
      else if(e.target.id === "clubRadio")
      {
         date.style.display = "none";
         league.style.display = "flex";
         leg.style.display = "none";
         season.style.display = "flex";
         club.style.display = "flex";
      }

      // if the selected one is for competition, setting display properties for non-used, so they are not visible
      else if(e.target.id === "leagueRadio")
      {
         season.style.display = "flex";
         league.style.display = "flex";
         leg.style.display = "flex";
         club.style.display = "none";
         date.style.display = "none";
      }

      // code if the league option input was changed
      else if(e.target.id === "searchLeague")
      {
         // if it is for Polish Cup, making the new options
         if(e.target.value === "Puchar Polski")
         {
            leg.innerHTML = `
            <select id="searchLeg" class="form__input">
            <option value="1_32">1/32 finału</option>
            <option value="1_16">1/16 finału</option>
            <option value="1_8">1/8 finału</option>
            <option value="1_4">1/4 finału</option>
            <option value="1_2">1/2 finału</option>
            <option value="1">Finał</option></select>
            <label for="searchLeg" class="form__label">Faza:</label>`;
         }
         else
         {
            leg.innerHTML = `<input type="number" id="searchLeg" min="1" max="38" class="form__input">
            <label for="searchLeg" class="form__label">Kolejka:</label>`;
            // if it is for supercup
            if(e.target.value === "Superpuchar")
            {
               // disabling the option to change
               leg.lastChild.disabled = "true";
            }
         }
      }
   });

   // submiting event
   timetableForm.addEventListener("submit", e =>
   {
      e.preventDefault(); // preventing reloading

      // selecting all radio buttons
      const clubRadio = document.querySelector("#clubRadio"); // radio button - option club
      const dateRadio = document.querySelector("#dateRadio"); // radio button - option date
      const leagueRadio = document.querySelector("#leagueRadio"); // radio button - option league

      // selecting all inputs
      const searchClub = document.querySelector("#searchClub"); // club
      const searchDate = document.querySelector("#searchDate"); // date
      const searchLeague = document.querySelector("#searchLeague"); // league
      const searchLeg = document.querySelector("#searchLeg"); // leg
      const searchSeason = document.querySelector("#searchSeason"); // season
      
      // selecting the box for the results
      const timetableMatches = document.querySelector("#timetableMatches");

      // making empty url
      let url = "";

      if(dateRadio.checked) // if radio button for date is selected
      {
         url = `/timetable/date/${searchDate.value}`; // changing url
      }

      if(clubRadio.checked) // if radio button for club is selected
      {
         url = `/timetable/season/${searchSeason.value}/club/${searchClub.value.split(" ").join("_")}`; // changing url
         if(searchLeague.value && searchLeague.value.length > 0) // if the league was also specified
         {
            url += `/competition/${searchLeague.value.split(" ").join("_")}`; // adding info about league
         }
      }

      if(leagueRadio.checked) // if radio button for league is selected
      {
         url = `/timetable/season/${searchSeason.value}/competition/${searchLeague.value.split(" ").join("_")}`; // changing url
         if(searchLeg.value && searchLeg.value.length > 0) // if match leg is specified
         {
            url += `/leg/${searchLeg.value}`; // adding info for url
         }
      }
      fetch(url) // making AJAX request to the url
      .then(res => res.json()) // fetching JSON response
      .then(data => showMatches(data, timetableMatches)) // calling the functions with received data
      .catch(err => alert("Wystąpił błąd")); // alerting if there was an error
   });
}