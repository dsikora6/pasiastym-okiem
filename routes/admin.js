// requiring necessary files

const   Comment     = require("../models/comment"),
        Entry       = require("../models/entry"),
        express     = require("express"),
        isAdmin     = require("../middlewares/isadmin"),
        User        = require("../models/user");

// setting express router
        
const   router      = express.Router({mergeParams: true});

router.get("/", isAdmin, (req, res) => // allow only for admins
{
   res.render("admin/index"); // show the site
});

router.get("/users", isAdmin, (req, res) => // allow only for admins
{
    User.find({}, (err, user) => // finding all users
    {
        if(err) // if there was an error
        {
            console.log(err); // throwing error to the console
            req.flash("error", "Niestety, wystąpił błąd."); // setting error message
            res.redirect("back"); // informing user about error, sending him back
        }
        else
        {
            res.render("admin/users", {users: user}); // show the site with users list
        }
    });
});

router.get("/users/:id", isAdmin, (req, res) => // allow only for admins
{
    User.findById(req.params.id, (err, user) => // find user with specified id
    {
        if(err) // if there was an error
        {
            console.log(err); // throwing error to the console
            req.flash("error", "Niestety, wystąpił błąd."); // setting error message
            res.redirect("back"); // informing user about error, sending him back
        }
        else
        {
            res.render("admin/useredit", {user: user}); // show the site with user info
        }
    });
});

router.put("/users/:id", isAdmin, (req, res) => // PUT request, only for admins
{

    // finding user with specified ID, updating with data from the form
    User.findByIdAndUpdate(req.params.id, req.body.user, (err) =>
    {
        if(err) // if there was an error
        {
            console.log(err); // throwing error to the console
            req.flash("error", "Niestety, wystąpił błąd."); // setting error message
            res.redirect("/admin/users"); // informing user about error, sending him back
        }
        else
        {
            res.redirect("/admin/users"); // redirecting to user list view
        }
    });
});

router.get("/comments", isAdmin, (req, res) => // only for admins
{
   Comment.find({}, (err, comment) => // finding all comments
   {
      if(err) // if there was an error
      {
            console.log(err); // throwing error to the console
            req.flash("error", "Niestety, wystąpił błąd."); // setting error message
            res.redirect("back"); // informing user about error, sending him back
      }
      else
      {
            res.render("admin/comments", {comments: comment}); // showing the page with all comments
      }
   });
});

router.get("/entry/:id/comment/:comid/edit", isAdmin, (req, res) => // only for admins
{
    Entry.findById(req.params.id, (err, entry) => // finding entry with specified ID
    {
        if(err) // if there was an error
         {
                console.log(err); // throwing error to the console
                req.flash("error", "Niestety, wystąpił błąd."); // setting error message
                res.redirect("back"); // informing user about error, sending him back
         }
        Comment.findById(req.params.comid, (err2, comment) => // finding comment with specified id
        {
            if(err2) // if there was an error
            {
                console.log(err2); // throwing error to the console
                req.flash("error", "Niestety, wystąpił błąd."); // setting error message
                res.redirect("back"); // informing user about error, sending him back
            }
            else
            {
                res.render("admin/commentedit", {entry: entry, comment: comment}); // showing info with comment and entry info
            }
        });
    });
});

router.put("/entry/:id/comment/:comid/", isAdmin, (req, res) => // PUT request , only for admins
{
   Entry.findById(req.params.id, (err, entry) => // finding entry with specified ID
   {
        if(err) // if there was an error
        {
            console.log(err); // throwing error to the console
            req.flash("error", "Niestety, wystąpił błąd."); // setting error message
            res.redirect("/"); // informing user about error, sending him back
        }
        else
        {
            // finding comment with specified ID, updating with form data
            Comment.findByIdAndUpdate(req.params.comid, req.body.comment, (err2) =>
            {
                if(err2) // if there was an error
                {
                    console.log(err2); // throwing error to the console
                    req.flash("error", "Niestety, wystąpił błąd."); // setting error message
                }
                res.redirect("/admin/comments"); // redictering to admin view
            });
        }
   });
});

// exporting routes
module.exports = router;