// requiring necessary items
const   Comment     = require("../models/comment"),
        Entry       = require("../models/entry"),
        express     = require("express"),
        isAdmin     = require("../middlewares/isadmin"),
        isLoggedIn  = require("../middlewares/isloggedin"),
        moment      = require("moment"),
        nodemailer  = require("nodemailer"),
        User        = require("../models/user");
        
// setting express router
const   router      = express.Router({mergeParams: true});

// setting mail service
const   transporter = nodemailer.createTransport({
    service: "gmail", // gmail
    auth: {
      user: process.env.SENDMAIL, // mail address
      pass: process.env.MAILPASS // mail password
    }
});

router.get("/new", isAdmin, (req, res) => // only for admins
{
   res.render("entry/new"); // showing the site
});

router.post("/", isAdmin, (req, res) => // POST request, only for admins
{
   req.body.entry.createdAt = moment().valueOf(); // setting the creation time to current UNIX time
   Entry.create(req.body.entry, (err, entry) => // creating entry with form data
   {
        if(err) // if there was an error
        {
            console.log(err); // throwing error to the console
            req.flash("error", "Niestety, wystąpił błąd."); // setting error message
            res.redirect("back"); // sending to the user, redictering back
        }
        else
        {
            res.redirect("/"); // redictering to main site
        }
   });
});

router.get("/id/:id", (req, res) =>
{
    Entry.findById(req.params.id, (err, entry) => // finding entry with specified ID
    {
        if(err) // if there was an error
        {
            console.log(err); // throwing error to the console
            req.flash("error", "Niestety, wystąpił błąd."); // setting error message
            res.redirect("/"); // sending to the user, redictering to main site
        }
        else
        {
            Comment.find({entry: entry}, (err2, comment) => // finding all comments to that entry
            {
                if(err) // if there was an error
                {
                    console.log(err2); // throwing error to the console
                    req.flash("error", "Niestety, wystąpił błąd."); // setting error message
                    res.redirect("/"); // sending to the user, redictering to main site
                }
                else
                {
                    // showing site with entry and comment data
                    res.render("entry/show", {entry: entry, moment: moment, comments: comment});
                }
            });
        }
    });
});

router.post("/id/:id/comment/new", isLoggedIn, (req, res) => // POST request, only for logged users
{
    req.body.comment = req.sanitize(req.body.comment); // sanitizing comment, just in case
    req.body.comment.createdAt = moment().valueOf(); // setting cration date to current UNIX time
    req.body.comment.entry = req.params.id; // setting the entry ID, so the comment could be found
    req.body.comment.accepted = false; // setting, that comment is not yet accepted
    
    Comment.create(req.body.comment, (err, comment) => // creating a comment with form data
    {
        if(err) // if there was an error
        {
            console.log(err); // throwing error to the console
            req.flash("error", "Niestety, wystąpił błąd."); // setting error message
            res.redirect("/"); // redictering to main site
        }
        else
        {
            if(req.user) { // if the user is logged in - double check
            User.findById(req.user._id, (err2, user) => // finding user with specified ID
            {
                if(err2) // if there was an error
                {
                    console.log(err2); // throwing error to the console
                    req.flash("error", "Niestety, wystąpił błąd."); // setting error message for user
                    res.redirect("/"); // redictering to main site
                }
                else
                {
                    user.comments.push(comment); // pushing comments to user comments array
                    user.save(); // saving the new comments array
                }
            });
            }
            
            Entry.findById(req.params.id, (err3, entry) => // finding entry with specified id
            {
                if(err3) // if there was an error
                {
                    console.log(err3); // throwing error to console
                    req.flash("error", "Niestety, wystąpił błąd."); // setting error message for user
                    res.redirect("/"); // redirecting to main site
                }
                else
                {
                    const mailOptions = { // settin mail options
                        from: process.env.SENDMAIL, // server mail address
                        to: process.env.RECMAIL, // receiver - my email
                        subject: "Pasiastym okiem - nowy komentarz", // mail subject
                        text: `Dodano nowy komentarz pod artykułem - ${entry.title}!` // mail content
                    };

                    transporter.sendMail(mailOptions, (error, info) => { // sending mail
                        if (error) { // if there was an error
                        console.log(error); // send info to console
                        } else {
                        console.log("Email sent: " + info.response); // send info to console
                        }
                    });
                    entry.comments.push(comment); // add comment to entry comment array
                    entry.save(); // save new array
                }
            });
            // set flash message for user
            req.flash("success", "Komentarz dodany. Musi jeszcze jedynie zostać zaakceptowany przez administratora");
            // redirect back
            res.redirect("back");
        }
        });
});

router.get("/id/:id/edit", isAdmin, (req, res) => // only for admins
{
   Entry.findById(req.params.id, (err, entry) => // finding entry with specified id
   {
        if(err) // if there was an error
        {
            console.log(err); // throwing error to the console
            req.flash("error", "Niestety, wystąpił błąd."); // setting error message for user
            res.redirect("back"); // redirecting back
        }
        else
        {
            res.render("entry/edit", {entry: entry}); // rendering site with entry data
        }
    });
});

router.put("/id/:id", isAdmin, (req, res) => // PUT request, only for admins
{
   Entry.findByIdAndUpdate(req.params.id, req.body.entry, (err) => // finding entry with ID and updating with form data
   {
        if(err) // if there was an error
        {
            console.log(err); // throw it to the console
            req.flash("error", "Niestety, wystąpił błąd."); // setting error message for user
        }
        res.redirect("/entry/id/" + req.params.id); // redirecting to the entry view
   });
});

router.delete("/id/:id", isAdmin, (req, res) => // DELETE request, only for admins
{
   Entry.findByIdAndRemove(req.params.id, (err) => // finding entry with specified ID
   {
        if(err) // if there was an error
        {
            console.log(err); // throw it to the console
            req.flash("error", "Niestety, wystąpił błąd."); // setting message for user
        }
        Comment.find({entry: req.params.id}, (err2, comment) => // finding comments related to that entry
        {
            if(err2) // if there was an error
            {
                console.log(err); // throw it to the console
                req.flash("error", "Niestety, wystąpił błąd."); // setting message for the user
            }
            else
            {
                comment.forEach(comm => // for each comment
                {
                    Comment.findByIdAndRemove(comm._id, err3 => // finding comment with ID and removing it
                    {
                        if(err3) // if there was an error
                        {
                            console.log(err3); // throw it to the console
                            req.flash("error", "Niestety, wystąpił błąd."); // setting message to the user
                        }
                    });
                });
            }
        });
        res.redirect("/"); // redirecting to main site
   });
});

router.get("/start/:start/amount/:amount", (req, res) =>
{
    Entry.find({}).sort({createdAt: "descending"}).exec((err, entries) => { // finding all entries
        if(err) // if there was an error
        {
            res.json(err); // return it
        }
        else
        {
            const length = entries.length; // getting all entries amount
            // setting start amount if is too great or too small
            if(length - req.params.start < req.params.amount) req.params.start = length - req.params.amount + 1;
            if(req.params.start < 1) req.params.start = 1;

            // getting x entries
            entries = entries.splice(req.params.start - 1, req.params.amount);
            res.json(entries); // returning it
        }
    });
});

router.get("/all", (req, res) =>
{
    Entry.find({}, (err, entries) => { // finding all entries
        if(err) // if there was an error
        {
            res.json(err); // return it
        }
        else
        {
            res.json(entries); // return entries
        }
    });
});

// export the router
module.exports = router;