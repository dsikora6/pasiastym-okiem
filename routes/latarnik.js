const express = require("express");
const router = express.Router();

router.get("/", (req, res) => 
{
    res.render("latarnik/main");
});

router.post("/", (req, res) => 
{
    let resp = [req.body.q1, req.body.q2, req.body.q3, req.body.q4, req.body.q5, req.body.q6, req.body.q7, req.body.q8, req.body.q9, req.body.q10,req.body.q11, req.body.q12, req.body.q13, req.body.q14, req.body.q15, req.body.q16, req.body.q17, req.body.q18,req.body.q19, req.body.q20];
    resp = resp.map(rs => {
        if(rs === "y0" || rs === "x0") return -3;
        if(rs === "y1" || rs === "x1") return -2;
        if(rs === "y2" || rs === "x2") return -1;
        if(rs === "y3" || rs === "x3") return 0;
        if(rs === "y4" || rs === "x4") return 1;
        if(rs === "y5" || rs === "x5") return 2;
        if(rs === "y6" || rs === "x6") return 3;
    });

    res.redirect("/latarnik/results?arr=" + resp);
});

router.get("/results", (req,res) =>
{
    res.render("latarnik/result", {resp: req.query.arr});
});

module.exports = router;