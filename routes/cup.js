const express = require("express");
const router = express.Router();

router.get("/", (req, res) => 
{
    res.render("cup/main");
});

router.get("/:id", (req, res) => 
{
    res.render("cup/" + req.params.id);
});

module.exports = router;