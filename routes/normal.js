// requiring necessary packages

const   Entry   = require("../models/entry"),
        express = require("express"),
        moment  = require("moment"),
        router  = express.Router();

router.get("/", (req, res) =>
{
    Entry.find({}).sort([["createdAt", "descending"]]).limit(1).exec((err, entry) => // finding all entries
    {
        if(err) // if there was an error
        {
            console.log(err); // throwing it to the console
            // setting the new entry with info
            entry = [[
                {
                    comments: [],
                    _id: "undefined",
                    title: "Niestety, wystąpił błąd",
                    text: "Niestety, wystąpił błąd. Proszę spróbować przeładować stronę. Jeśli problem nie zostanie rozwiązany, proszę skorzystać z menu.",
                    createdAt: moment().valueOf()
                }
            ]];
        }
        res.render("index", {entry: entry[0], moment: moment}); // rendering the site
    });
});

// exporting the router
module.exports = router;