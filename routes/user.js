// requiring necessary packages
const   express     = require("express"),
        isLoggedIn  = require("../middlewares/isloggedin"),
        nodemailer  = require("nodemailer"),
        passport    = require("passport"),
        User        = require("../models/user");
        
// setting router
const   router      = express.Router();

// setting mail service
const   transporter = nodemailer.createTransport({
    service: "gmail", // gmail
    auth: {
      user: process.env.SENDMAIL, // email address
      pass: process.env.MAILPASS // password
    }
  });

const checkCharacters = (word) => // function checking the word
{
    // array of all allowed characters
    const characters = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "w", "x", "y", "z", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "-", "_", "+", "=", "[", "{", "}", "]", ":", "|", "<", ">", ",", ".", "/", "?", "~"];
    word = word.toLowerCase().split(""); // changing each letter to lowercase, splitting to individual characters
    let length = 0; // setting counter to 0
    word.forEach(char => { // checking each character
        if (characters.indexOf(char) !== -1) length++; // if the character is in array, increment the counter
    });
    word.join(""); // join word
    return length === word.length; // return true, if the counter amount match word length
};

router.get("/register", (req, res) =>
{
    res.render("user/register"); // render the site
});


router.post("/register", (req, res) => // POST request
{
    /* checking conditions, setting message and redictering back if they are true */
    if(!req.body.username || req.body.username.length === 0) // check the username
    {
        req.flash("error", "Nie wprowadzono nazwy użytkownika");
        res.redirect("back");
    }

    else if(req.body.username.length < 3 || req.body.username.length > 20) // check username length
    {
        req.flash("error", "Nazwa użytkownika musi zawierać między 3 a 20 znaków");
        res.redirect("back");
    }

    else if(!req.body.password || req.body.password.length === 0) // check password
    {
        req.flash("error", "Nie wprowadzono hasła");
        res.redirect("back");
    }

    else if(req.body.password.length < 8 || req.body.password.length > 44) // check password length
    {
        req.flash("error", "Hasło musi zawierać między 8 a 44 znaki");
        res.redirect("back");
    }

    else if(!checkCharacters(req.body.username)) // check characters in username
    {
        req.flash("error", "Nazwa użytkownika zawiera niedozwolone znaki");
        res.redirect("back");
    }

    else if(!checkCharacters(req.body.password)) // check characters in password
    {
        req.flash("error", "Hasło zawiera niedozwolone znaki");
        res.redirect("back");
    }

    else if(typeof req.body.password !== "string" || typeof req.body.username !== "string") // check username/password type
    {
        req.flash("error", "Nieprawidłowy typ hasła.");
        res.redirect("back");
    }

    else
    {
        req.body.username = req.sanitize(req.body.username); // sanitize username
        req.body.password = req.sanitize(req.body.password); // sanitize password
        // create new user with user rank
        User.register(new User({username: req.body.username, rank: "User", accepted: false}), req.body.password, (err, user) =>
        {
            if(err) // if there is an error
            {
                console.log(err); // throw it to console
                if(err.name === "UserExistsError")
                {
                    err.message = "Nazwa użytkownika jest już zajęta!"; // setting a message if the user exist
                }
                req.flash("error", err.message); // showing message to the user
                res.redirect("back"); // redirecting back
            }
            passport.authenticate("local")(req, res, () => // authenticating user, creating hashes
            {
                console.log(user); // send user data to console
                const mailOptions = { // set mail options
                    from: process.env.SENDMAIL, // sender email
                    to: process.env.RECMAIL, // receiver email
                    subject: "Pasiastym okiem - nowy użytkownik", // subject
                    text: `Zarejestrowano nowego użytkownika - ${req.body.username}!` // text content
                };
                transporter.sendMail(mailOptions, (error, info) => { // sending mail
                    if (error) {
                    console.log(error); // throw error if there is one
                    } else {
                    console.log("Email sent: " + info.response); // send an info to console
                    }
                });

                // send message to user
                req.flash("success", req.user.username + ", zostałeś/aś pomyślnie zarejestrowany/a!. Twoje konto musi zostać aktywowane przez administratora.");
                res.redirect("/"); // redirect to main site
            });
        });
    }
});

router.get("/login", (req, res) =>
{
    res.render("user/login"); // render the site
});

router.post("/login", passport.authenticate("local", // POST request, authentication
{
    successRedirect: "/", // if success, redirect to main site
    failureRedirect: "/login", // if error, redirect to login site
    failureFlash: "Zła nazwa użytkownika lub hasło" // set the message
}), (req, res) => {});

router.get("/logout", (req, res) =>
{
    req.logout(); // logging out the user
    res.redirect("/"); // redirecting to main page
});

router.get("/changepassword", isLoggedIn, (req, res) => // only for logged users
{
    res.render("user/change"); // rendering the page
});

router.post("/changepassword", isLoggedIn, (req, res) =>
{
    /* checking conditions, setting message and redictering back if they are true */
    if(req.body.password !== req.body.passwordConfirmation) // are passwords the same
    {
        req.flash("error", "Hasła są różne!");
        res.redirect("back");
    }

    else if(!req.body.password || req.body.password.length === 0) // check password
    {
        req.flash("error", "Nie wprowadzono hasła");
        res.redirect("back");
    }

    else if(req.body.password.length < 8 || req.body.password.length > 44) // check password length
    {
        req.flash("error", "Hasło musi zawierać między 8 a 44 znaki");
        res.redirect("back");
    }

    else if(!checkCharacters(req.body.password)) // check characters in password
    {
        req.flash("error", "Hasło zawiera niedozwolone znaki");
        res.redirect("back");
    }

    else if(typeof req.body.password !== "string") // check password type
    {
        req.flash("error", "Nieprawidłowy typ hasła.");
        res.redirect("back");
    }

    else
    {
        req.body.password = req.sanitize(req.body.password); // sanitizing the password
        User.findById(req.user._id, (err, user) => { // finding user
            if(err) // if error
            {
                    req.flash("error", "Wystąpił błąd"); // send message to user
                    res.redirect("/"); // redirect to main page
            }
            else
            {
                user.setPassword(req.body.password, (error, u) => { // changing password
                    if(error) // if error
                    {
                        req.flash("error", "Wystąpił błąd"); // send message to user
                        res.redirect("/"); // redirect to main page
                    }
                    else
                    {                    
                        u.save(); // save the password
                        req.flash("success", "Zmiana powiodła się"); // send message to user
                        res.redirect("/"); // redirect to main page
                    }
                });
            }
        })
    }
});

// export the router
module.exports = router;