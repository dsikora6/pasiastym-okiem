// requiring necessary packages
const   express = require("express"),
        isAdmin = require("../middlewares/isadmin"),
        latinize = require("latinize"),
        Match   = require("../models/match"),
        moment  = require("moment"),
        // setting router
        router  = express.Router();

router.get("/", (req, res) => 
{
    res.render("timetable/main"); // rendering the site
});

router.get("/new", isAdmin, (req, res) => // only for admins
{
    res.render("timetable/new"); // render site
});

router.post("/", isAdmin, (req, res) => // POST request, only for admins
{
    req.body.match.unixDate = moment(req.body.match.date).valueOf(); // setting unix date
    req.body.match.date = moment(req.body.match.date).format("YYYY-MM-DD"); // setting date in YYYY-MM-DD format (ex. 2021-03-14)
    req.body.match.teams = [req.body.match.hostTeam, req.body.match.awayTeam];  // making array

    // latinizing the names, removing spaces and hyphens, making the logo urls
    let hostName = latinize(req.body.match.hostTeam.split(" ").join("").split("-").join("").toLowerCase());
    req.body.match.hostTeamEmblem = `${hostName}.png`;
    let awayName = latinize(req.body.match.awayTeam.split(" ").join("").split("-").join("").toLowerCase());
    req.body.match.awayTeamEmblem = `${awayName}.png`;
    let competitionLogo = latinize(req.body.match.competition.split(" ").join("").toLowerCase());
    req.body.match.competitionImage = `${competitionLogo}.png`;

    req.body.match.images = [req.body.match.hostTeamEmblem, req.body.match.awayTeamEmblem]; // making array
    Match.create(req.body.match, (err, entry) => // creating an entry
    {
        if(err) // if there was an error
        {
            console.log(err); // throw it to the console
            req.flash("error", "Niestety, wystąpił błąd."); // setting message for user
            res.redirect("back"); // redirecting back
        }
        else
        {
            res.redirect("/timetable"); // redirecting to the timetable view
        }
    });
});

router.get("/manage", isAdmin, (req, res) => { // only for admins
    res.render("timetable/manage"); // render the site
});

router.get("/id/:id/edit", isAdmin, (req, res) => // only for admins
{
    Match.findById(req.params.id, (err, match) => // find match with specified ID
    {
        if(err) // if there was an error
        {
            console.log(err); // throw it to the console
            req.flash("error", "Niestety, wystąpił błąd."); // setting message for user
            res.redirect("back"); // redictering back
        }
        else
        {
            res.render("timetable/edit", {match: match, moment: moment}); // rendering site with match data
        }
    });
});

router.put("/id/:id", isAdmin, (req, res) => // PUT request, only for admins
{
    req.body.match.unixDate = moment(req.body.match.date).valueOf(); // setting unix date
    req.body.match.date = moment(req.body.match.date).format("YYYY-MM-DD"); // setting date in YYYY-MM-DD format (ex. 2021-03-14)
    req.body.match.teams = [req.body.match.hostTeam, req.body.match.awayTeam]; // making an array

    // latinizing the names, removing spaces and hyphens, making the logo urls
    let hostName = latinize(req.body.match.hostTeam.split(" ").join("").split("-").join("").toLowerCase());
    req.body.match.hostTeamEmblem = `${hostName}.png`;
    let awayName = latinize(req.body.match.awayTeam.split(" ").join("").split("-").join("").toLowerCase());
    req.body.match.awayTeamEmblem = `${awayName}.png`;
    let competitionLogo = latinize(req.body.match.competition.split(" ").join("").toLowerCase());
    req.body.match.competitionImage = `${competitionLogo}.png`;

    req.body.match.images = [req.body.match.hostTeamEmblem, req.body.match.awayTeamEmblem]; // making an array

    Match.findByIdAndUpdate(req.params.id, req.body.match, (err) => // finding entry with specified id, updating it with form data
    {
        if(err) // if there was an error
        {
            console.log(err); // throw it to the console
            req.flash("error", "Niestety, wystąpił błąd."); // setting message for the user
        }
        res.redirect("/timetable/manage"); // redirecting to manage page
    }) ;
});

router.delete("/id/:id", isAdmin, (req, res) => // DELETE request, only for admins
{
    Match.findByIdAndRemove(req.params.id, (err) => // finding and removing entry with specified id
    {
        if(err) // if there was an error
        {
            console.log(err); // throw it to the console
            req.flash("error", "Niestety, wystąpił błąd."); // setting message for user
        }
        res.redirect("/timetable/manage"); // redirecting to admin route
    });
});

router.get("/date/:date",(req, res) =>
{
    Match.find({date: req.params.date}, (err, matches) => // finding matches with the same date
    {
        if(err) // if there was an error
        {
            console.log(err); // throw it to the console
            res.json(err); // return it
        }
        else
        {
            res.json(matches); // return matches list
        }
    });
});

router.get("/next/:date",(req, res) =>
{
    let date = ""; // empty string for date
    let amount = 0; // resetting amount to 0
    if(req.params.date.indexOf("_") !== -1) // if there is an underscore in URL
    {
        req.params.date = req.params.date.split("_"); // splitting at underscore
        date = req.params.date[0]; // setting date to the first element
        amount = parseInt(req.params.date[1]); // setting second element as amount
    }
    else
    {
        date = req.params.date; // setting date as in URL
        amount = 100; // setting amount to 100
    }

    // finding matches after the date, sorting it, and returning only the first x
    Match.find({unixDate: {$gte: date}}).sort({unixDate: "ascending"}).limit(amount).exec((err, matches) =>
    {
        if(err) // if there was an error
        {
            console.log(err); // throw it to the console
            res.json(err); // return it
        }
        else
        {
            res.json(matches); // return array
        }
    });
});

router.get("/season/:season/club/:club", (req, res) =>
{
    let club = req.params.club.split("_").map(part => { // splitting club name at underscore
        if(part.indexOf("-") !== -1) // if there is a hyphen
        {
            part = part.split("-").map(p => { // splitting at hyphen
                return p = p.charAt(0).toUpperCase() + p.slice(1); // changing first letter of each part to uppercase
            }).join("-"); // joining with hyphen
        }
        return part.charAt(0).toUpperCase() + part.slice(1); // setting first letter to uppercase
    }).join(" "); // joining with space
    
    Match.find({season: req.params.season, teams: club}, (err, matches) => // finding match from the season and with this club
    {
        if(err) // if there was an error
        {
            console.log(err); // throw it to the console
            res.json(err); // return it
        }
        else
        {
            res.json(matches); // return array
        };
    });
});

router.get("/season/:season/club/:club/competition/:competition", (req, res) =>
{
    let club = req.params.club.split("_").map(part => { // splitting club name at underscore
        if(part.indexOf("-") !== -1) // if there is a hyphen
        {
            part = part.split("-").map(p => {  // splitting at hyphen
                return p = p.charAt(0).toUpperCase() + p.slice(1); // changing first letter of each part to uppercase
            }).join("-"); // joining with hyphen
        }
        return part.charAt(0).toUpperCase() + part.slice(1); // setting first letter to uppercase
    }).join(" "); // joining with space

    let competition = req.params.competition.split("_").map(part => { // splitting competition name at underscore
        if(part.indexOf("i") === 0) // if starts with i
        {
            part = part.toUpperCase(); // changing all letters to uppercase
        }
        return part.charAt(0).toUpperCase() + part.slice(1); // changing first letter of each part to uppercase
    }).join(" "); // joining with space
    
    // finding all matches of this club in this competitions in this season
    Match.find({season: req.params.season, teams: club, competition: competition}, (err, matches) =>
    {
        if(err) // if there was an error
        {
            console.log(err); // throw it to the console
            res.json(err); // return it
        }
        else
        {
            res.json(matches); // return array
        };
    });
});

router.get("/season/:season/competition/:competition", (req, res) =>
{
    let competition = req.params.competition.split("_").map(part => { // split competition name at underscore
        if(part.indexOf("i") === 0) // if first letter is i
        {
            part = part.toUpperCase(); // change all letters to uppercase
        }
        return part.charAt(0).toUpperCase() + part.slice(1); // change first letter to uppercase
    }).join(" "); // join with space

    // find all matches from that season in this competition
    Match.find({season: req.params.season, competition: competition}, (err, matches) =>
    {
        if(err) // if there was an error
        {
            console.log(err); // throw it to the console
            res.json(err); // return it
        }
        else
        {
            res.json(matches); // return array
        };
    });
});

router.get("/season/:season/competition/:competition/leg/:leg", (req, res) =>
{
    let competition = req.params.competition.split("_").map(part => { // split competition name at underscore
        if(part.indexOf("i") === 0 || part.indexOf("clj") === 0) // if starts with i or contains clj
        {
            part = part.toUpperCase(); // change all letters to uppercase
        }
        return part.charAt(0).toUpperCase() + part.slice(1); // change first letter to uppercase
    }).join(" "); // join with space

    let leg = ""; // set empty string
    if(competition === "Puchar Polski")
    {
        if(req.params.leg.length > 1) leg = `${req.params.leg.split("_").join("/")} finału`; // if length of the value is bigger than 1, split at underscore and join with slash
        else leg = "Finał"; // else set to final
    }
    else leg = `${req.params.leg} kolejka`; // else set the name

    // find all matches of that season in this competion in this leg
    Match.find({season: req.params.season, competition: competition, description: leg}, (err, matches) =>
    {
        if(err)
        {
            console.log(err); // throw an error to console
            res.json(err); // return it
        }
        else
        {
            res.json(matches); // return array
        };
    });
});

// export router
module.exports = router;